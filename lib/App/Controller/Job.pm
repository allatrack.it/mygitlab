package App::Controller::Job;

use Mojo::Base 'Mojolicious::Controller';

sub index {
    my $self = shift;
    $self->res->headers->content_type("text/plain");
    my $res = `perl /var/www/mygitlab/mygitlab cmds`;
    return $self->render( text => $res );
}


sub cmd {
    my $self = shift;
    
    $self->res->headers->content_type("text/plain");

    my $allow = { cmds => 1, ng_build_allatrack => 1, routes => 1, version => 1 };

    my $cmd = $self->stash->{cmd} || 'cmds';

    return $self->render( json => { error => 'Cmd is not allowed' }, status => 404 )
        unless exists $allow->{$cmd};
        
    return $self->render( text => 'Busy for ' . $cmd ) if $self->app->{redis}->get($cmd);

    $self->_startJob($cmd);

    my $res = `perl /var/www/mygitlab/mygitlab $cmd &`;

    # $self->app->{redis}->del($cmd);

    return $self->render( text =>  $cmd . ' RUN ' . $res ); 
}

sub _startJob {
    my $self = shift;
    my $arg  = shift or return 0;
    $self->app->{redis}->set( $arg => `date` );
    $self->app->{redis}->expire( $arg => 300 );   
    $self->app->{redis}->get( $arg );
}


# sub gitlab {
#     my $self  = shift;
# 
#     if($self->req->headers->header('X-Gitlab-Token') eq @{$self->app->config('secrets')}[0]) {
#         my $arg = $self->req->json;
# 
#         $self->render(text => Dumper($arg->{project}), code => 200);
#     } else {
#         warn 'BAD TOKEN';
# 
#         $self->render(text => 'BAD TOKEN', code => 401);
#     }
# }


1;
