package Command::cmds;

use Mojo::Base 'Mojolicious::Command';

has description => 'Show all available commands';
has usage 	=> 'USAGE: cmds';

sub run {
    my $self = shift;
    
    print '
    	USAGE EXAMPLE:
    	
    	http://.../job/routes

    	COMMANDS:

        cmds                  Show commands
        ng_build_allatrack    Get fresh code from gitlab and buid to web dir
        routes                Show available routes
        version               Show versions of available modules

';

}

1;

=cat

=head1 TODO

This information should be present dynamically from the special controller method.

