package Command::ng_build_allatrack;

use Mojo::Base 'Mojolicious::Command';

use Time::Piece;

has usage       => 'USAGE: ng_build_allatrack';
has description => 'get fresh code from gitlab and buid to web dir';

sub run {
    my $self       = shift;

    my $repo       = 'git@gitlab.com:allatrack-web/allatrack-web-v2.git';
    my $branch     = 'development';
    my $path_src   = '/var/src/allatrack-web-v2/';
    my $path_web   = '/var/www/web2.allatrack.com/';
    my $path_dist  = $path_src.'dist/allatrack/browser';
    my $nmods_data = '/var/src/node_modules/';
    my $nmods_src  = $path_src.'node_modules/';

    say "* BUILDING\n";

    my $cmd = '';

    $cmd = "rm -rf $nmods_data"; 
    say "* $cmd if -d $nmods_src";
    say `$cmd`   if -d $nmods_src;

    $cmd = "cp -r $nmods_src $nmods_data"; 
    say "* $cmd if -d $nmods_src";
    say `$cmd`   if -d $nmods_src;

    $cmd = "rm -rf $path_src"; 
    say "* $cmd if -d $path_src";
    say `$cmd`   if -d $path_src;

    $cmd = "git clone -b $branch $repo $path_src"; 
    say "* $cmd \n";
    say `$cmd`;

    say "* $nmods_data exists for copy to $nmods_src. GOOD!." if -d $nmods_data;

    $cmd = "cp -r $nmods_data $nmods_src"; 
    say "* $cmd";
    say `$cmd`;

    die("* $nmods_src not exists. STOP! ") unless -d $nmods_src;

    $cmd = "npm run build --prefix $path_src"; 
    say "* $cmd";
    say `$cmd`;

    $cmd = "rm -rf $path_web"; 
    say "* $cmd";
    say `$cmd` if -d $path_web and -d $path_dist;

    $cmd = "mv $path_dist $path_web"; 
    say "* $cmd";
    say `$cmd` if -d $path_dist;

    dateup($path_web.'index.html');

    say "* DONE\n";
}

sub dateup { 
    `echo '<!-- Build ' \$(date --rfc-3339=ns) ' -->' >> $_[0]` if -e $_[0]; 
}


1;

=cat

=head1 TODO

Multi

Each repository should be configurable from special ini file
